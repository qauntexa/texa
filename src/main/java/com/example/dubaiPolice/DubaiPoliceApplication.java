package com.example.dubaiPolice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DubaiPoliceApplication {

	public static void main(String[] args) {
		SpringApplication.run(DubaiPoliceApplication.class, args);
	}

}
